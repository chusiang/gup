.PHONY: main init run status clean deploy_to_pages

main: run

init:
	docker-compose pull

run:
	docker-compose up -d

status:
	docker-compose ps

clean:
	docker-compose stop gup
	docker-compose rm -f gup
	rm -rf public/

deploy_to_pages:
	mkdir 	public/
	cp -r images javascripts stylesheets public/
	cp *.html *.json public/
