# ./gup --help

Get Ubuntu Packages with apturl and web, version 2.0

* Website: http://gup.drx.tw/

> This project has be migrated from https://github.com/chusiang/gup .

## Description:

1. Install package with single.

 * Click package name.

2. Install package with batch.

 * Click Install after click some checkbox.

3. Maintain package list with [./packages.json](./packages.json).

## License

Coypright © from 2015 chusiang GPL v2 or later.

